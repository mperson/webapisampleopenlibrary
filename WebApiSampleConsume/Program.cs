﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiSampleConsume.Tools.Models;
using WebApiSampleConsume.Tools;

namespace WebApiSampleConsume
{
    class Program
    {
        static void Main(string[] args)
        {
            OpenLibraryRequester ORequester = new OpenLibraryRequester();

            bookModel bm = ORequester.GetInfos("0451526538");

            if(bm != null)
            {
                Console.WriteLine("Titre : {0}", bm.title);
                Console.WriteLine("Date de publication: {0}",bm.publish_date);
                Console.WriteLine("Auteur:");
                    foreach(Author a in bm.authors)
                {
                    Console.WriteLine("  -"+a.name);
                }
                Console.WriteLine("Covers(S):{0}", bm.cover.small);
                Console.WriteLine("Covers(M):{0}", bm.cover.medium);
                Console.WriteLine("Covers(L):{0}", bm.cover.large);

            }

            Console.ReadKey();
        }
    }
}
